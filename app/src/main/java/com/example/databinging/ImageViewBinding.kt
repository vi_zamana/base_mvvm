package com.example.databinging

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

object ImageViewBinding {

    @BindingAdapter("imageUrl")
    fun ImageView.loadImage(url: String) {
        if (url.isNotEmpty()) {
            Glide.with(this)
                .load(url)
                .apply(RequestOptions().centerCrop())
                .into(this)
        }
    }
}