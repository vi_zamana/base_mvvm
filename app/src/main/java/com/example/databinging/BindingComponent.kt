package com.example.databinging

import androidx.databinding.DataBindingComponent

class BindingComponent : DataBindingComponent {

    override fun getImageViewBinding() = ImageViewBinding
}