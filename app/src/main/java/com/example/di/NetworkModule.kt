package com.example.di

import android.util.Log
import com.example.repository.RequestsApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val TIMEOUT_SEC = 30L

const val BASE_URL = ""

@Module
class NetworkModule {

    @Provides
    fun provideRequestsApi()
            : RequestsApi {
        val okHttpClient = HttpClientFactory()
            .createHttpClient()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create<RequestsApi>(RequestsApi::class.java)
    }

    inner class HttpClientFactory() {
        fun createHttpClient(): OkHttpClient {
            val builder = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor { message ->
                    Log.d("OkHttp", message)
                }.apply { level = HttpLoggingInterceptor.Level.BODY })
                .protocols(listOf(Protocol.HTTP_2, Protocol.HTTP_1_1))
                .connectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_SEC, TimeUnit.SECONDS)
            return builder.build()
        }
    }
}