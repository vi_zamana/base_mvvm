package com.example.di

import android.content.Context
import com.example.MainApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: MainApplication) {

    @Provides
    @Singleton
    fun provideApplication(): MainApplication = app

    @Provides
    @Singleton
    fun provideApplicationContext(app: MainApplication): Context = app
}