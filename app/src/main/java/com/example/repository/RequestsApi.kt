package com.example.repository

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.PUT


interface RequestsApi {

    @PUT("testUrl")
    fun request(): Single<ResponseBody>
}