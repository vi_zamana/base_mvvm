package com.example

import android.app.Application
import androidx.databinding.DataBindingUtil
import com.example.databinging.BindingComponent
import com.example.di.AppModule
import com.example.di.ApplicationComponent
import com.example.di.DaggerApplicationComponent

class MainApplication : Application() {

    companion object {
        lateinit var appComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        DataBindingUtil.setDefaultComponent(BindingComponent())
        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}